resource "aws_security_group" "var_demo" {
    name   = "my-security-group"

    ingress {
      from_port = 443
      to_port = 443
      protocol   = "tcp"
      #cidr_block = ["${aws_eip.lb.public_ip}/32"]
     cidr_blocks = [var.vpn_ip]
    }

    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = [var.vpn_ip]
    }

    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [var.vpn_ip]
    }
}
