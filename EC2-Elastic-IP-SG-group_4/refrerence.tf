resource "aws_instance" "myec2" {
    ami = "ami-02354e95b39ca8dec"
    instance_type = "t2.micro"
}

resource "aws_eip" "lb" {
    vpc  = true
}

resource "aws_eip_association" "eip_assoc" {
    instance_id   = aws_instance.myec2.id
    allocation_id = aws_eip.lb.id
}

resource "aws_security_group" "allow_tls" {
    name   = "my-security-group"

    ingress {
      from_port = 443
      to_port = 443
      protocol   = "tcp"
      #cidr_block = ["${aws_eip.lb.public_ip}/32"]
     cidr_blocks = [aws_eip.lb.public_ip/32]
    }

    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["aws_eip.lb.public_ip"]
    }

    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["aws_eip.lb.public_ip"]
    }
}
