variable "istest" {}

resource "aws_instance" "dev" {
  ami = "ami-02354e95b39ca8dec"
  instance_type = "t2.micro"
  count = var.istest == true ? 1 : 0
}

resource "aws_instance" "prod" {
  ami = "ami-02354e95b39ca8dec"
  instance_type = "t2.micro"
  count = var.istest == false ? 1 : 0
}
